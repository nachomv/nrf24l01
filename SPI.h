/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef HEADER_SPI
#define	HEADER_SPI

void init_SPI(void);
unsigned char ucBufferStatus( void );
unsigned char ucWriteCollision( void );
unsigned char ucReceiveOverflow( void );

unsigned char spiSendByte( unsigned char ucData );
//unsigned char spiReadNBytes( unsigned int numBytes );
unsigned char spiRead2Bytes( int *iFirst, int *iSecond );
unsigned char spiWriteRegister( unsigned char ucRegister, unsigned char ucData );
void spiWait( void );
unsigned int uiSPIDataReady( void );
unsigned char ucSPIRead( void );
void spiCSActive( void );
void spiCSInactive( void );

void spiReadRegister( unsigned char ucRegister, int numBytes );
void spiSendDummyByte( void );

#ifdef	__cplusplus
extern "C" {
#endif 
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	

