/*
 * File:   main.c
 * Author: imendizabal
 *
 * Created on 2 de febrero de 2019, 18:51
 */


#include <xc.h>
#include <pic16f18446.h>

#include "IO.h"
#include "SPI.h"
#include "Time.h"
#include "NRF24L01.h"
#include "main.h"


void main(void) 
{  
    init_Watchdog();
    init_Power();  
    init_Clock();
    init_PMD();
    init_IO();   
    init_SPI();
    init_Timers();  
    init_Interrupts();   
    delayms(1);
    
    RF_readStatus();
    delayms(1);
    initTransceiver();
    
    while(1)
    {
        RF_readStatus();
        delayms(1);
        RF_sendByte( 0xAA );
    }
}

void init_Watchdog(void)
{
    WDTCON0bits.SEN = 0;
}

/**
 * Power configuration modules
 */

void init_Power(void)
{
   VREGCON &= 0xFD;
   WDTCON1 = 0x0;
   CPUDOZE = 0x00;
   WDTPSH  = 0xFF;
   WDTPSL  = 0xFF;
   delayms(10);
}

/**
 * Initializes clock modules
 */
  
void init_Clock(void)
{
    OSCCON1 = 0x60;  /* HFINTOSC */
    OSCCON2 = 0x60;  /* HFINTOSC */
    OSCCON3 = 0x40;
    OSCFRQ  = 0x03; /* 8 MHz */
    OSCTUNE = 0x00;    
    OSCEN   = 0xFF;
    delayms(10);
}

void init_Timers(void)
{   
    init_Timer0();
    start_Timer0();
}

void init_Communications(void)
{
    init_SPI();
}

void init_Interrupts(void)
{
    PIE0bits.TMR0IE  = 1; /* Timer0 Interrupt Enable */
    PIR0bits.TMR0IF  = 0; /* Timer0 Flag Clear */
    INTCONbits.GIE   = 1; /* Global interrupts enabled */
    delayms(10);
}

void __interrupt() isr(void)
{
    /* Timer0 interrupt: Toggle LED0 */
   if( PIR0bits.TMR0IF && PIE0bits.TMR0IE )
   {
       toggleLED(0);
       PIR0bits.TMR0IF = 0;
   }
}
