/*
 * File:   IO.c
 * Author: imendizabal
 *
 * Created on 2 de febrero de 2019, 18:56
 */


#include <xc.h>
#include <pic16f18446.h>
#include "IO.h"

 /*
 * Peripheral Module Disable (PMD) initialization
 */
void init_PMD(void)
{
    PMD0 = 0x00;  /*ENABLED: CLKRMD, CLKR, SYSCMD, SYSCLK, FVRMD, FVR, IOCMD, IOC, NVMMD, NVM */
    PMD1 = 0xFC; /* ENABLED: TMR0MD, TMR0; DISABLED: TMR1MD, TMR1, TMR4MD, TMR4, TMR5MD, TMR5 TMR2MD TMR2, TMR3MD, TMR3, enabled, TMR6MD, TMR6 */
    PMD6 = 0x00; /* ENABLED: U1MD, EUSART1, MSSP1MD, MSSP1, MSSP2MD, MSSP2 */
}

/**
 * I/O initialization
 */
void init_IO(void)
{
    /* Input or Output Configuration */
    TRISAbits.TRISA1 = OUTPUT;
    TRISAbits.TRISA2 = OUTPUT;
    TRISAbits.TRISA4 = OUTPUT;
    TRISAbits.TRISA5 = OUTPUT;
    
    TRISBbits.TRISB4 = OUTPUT;
    TRISBbits.TRISB5 = OUTPUT;
    TRISBbits.TRISB6 = OUTPUT;
    TRISBbits.TRISB7 = OUTPUT;
    
    TRISCbits.TRISC0 = OUTPUT;
    TRISCbits.TRISC1 = OUTPUT;
    TRISCbits.TRISC2 = OUTPUT;
    TRISCbits.TRISC3 = OUTPUT;
    TRISCbits.TRISC4 = OUTPUT;   
    TRISCbits.TRISC5 = INPUT;    
    TRISCbits.TRISC6 = OUTPUT;   
    TRISCbits.TRISC7 = OUTPUT;
    
    LATAbits.LATA1   = 0;
    LATAbits.LATA2   = 0;
    LATAbits.LATA4   = 0;
    LATAbits.LATA5   = 0;
   
    LATBbits.LATB4 = 0;
    LATBbits.LATB5 = 0;
    LATBbits.LATB6 = 0;
    LATBbits.LATB7 = 0;
    
    LATCbits.LATC0 = 0;
    LATCbits.LATC1 = 0;
    LATCbits.LATC2 = 0;
    LATCbits.LATC3 = 0;
    LATCbits.LATC4 = 0;
    LATCbits.LATC6 = 0;
    LATCbits.LATC7 = 0;
        
    
     /* Analog input or Digital I/O Configuration */
      ANSELC = 0x00;
      ANSELB = 0x00;
      ANSELA = 0x00;
       
      /* Weak Pull-up Configuration */
      WPUB = 0xFF; /* Disabled */
      WPUA = 0xFF; /* Disabled */
      WPUC = 0xFF; /* Disabled */
    
     /* Open-Drain or push-pull configuration */
      ODCONA = 0x00; /* push-pull */
      ODCONB = 0x00; /* push-pull */
      ODCONC = 0x00; /* push-pull */
      
      /* Slew-Rate Configuration */
      SLRCONA = 0x00; /* Maximum rate */
      SLRCONB = 0x00; /* Maximum rate */
      SLRCONC = 0x00; /* Maximum rate */
 
      RB6PPS = 0x0F; /* RB6 UART TX -> Output */
      RX1PPS = 0x0C; /* RB4 UART RX -> Input */
      
      /** DEBUG BOARD **/ 
      RC4PPS     = 0x14; /* MOSI to RC4 -> Output */
      RC6PPS     = 0x13; /* SCK to RC6  -> Output */
      SSP1CLKPPS = 0x16; /* SCK to RC6  -> Input  */
      SSP1DATPPS = 0x15; /* MISO to RC5 -> Input  */
}

void LED_ON( int numLED )
{
    if( numLED == 0 )
    {
        LED0_LAT = LEDST_ON;
    }
}

void LED_OFF( int numLED )
{
    if( numLED == 0 )
    {
        LED0_LAT = LEDST_OFF;
    }
}

void toggleLED( int numLED )
{
    if( numLED == 0 )
    {
        LATAbits.LATA2 = ~LATAbits.LATA2;
    }
}

void RC7ON(void)
{
    LATCbits.LATC7 = ON;
}

void RC7OFF(void)
{
    LATCbits.LATC7 = OFF;
}

void RA5ON( void )
{
    LATAbits.LATA5 = ON;
}

void RA5OFF( void )
{
    LATAbits.LATA5 = OFF;
}

void RELAY_OPEN( void )
{
    LATBbits.LATB7 = ON;
}

void RELAY_CLOSE( void )
{
    LATBbits.LATB7 = OFF;
}
