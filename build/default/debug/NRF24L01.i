# 1 "NRF24L01.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "C:\\Program Files (x86)\\Microchip\\xc8\\v2.00\\pic\\include\\language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "NRF24L01.c" 2







# 1 "./NRF24L01.h" 1
# 56 "./NRF24L01.h"
void initTransceiver( void );
void ceON( void );
void ceOFF( void );
void RF_sendSPICommand( unsigned char ucCommand );
# 8 "NRF24L01.c" 2

# 1 "./SPI.h" 1
# 34 "./SPI.h"
void init_SPI(void);
unsigned char ucBufferStatus( void );
unsigned char ucWriteCollision( void );
unsigned char ucReceiveOverflow( void );

unsigned char spiSendByte( unsigned char ucData );

unsigned char spiRead2Bytes( int *iFirst, int *iSecond );
unsigned char spiWriteRegister( unsigned char ucRegister, unsigned char ucData );
void spiWait( void );
unsigned int uiSPIDataReady( void );
unsigned char ucSPIRead( void );
void spiCSActive( void );
void spiCSInactive( void );

void spiReadRegister( unsigned char ucRegister, int numBytes );
void spiSendDummyByte( void );
# 9 "NRF24L01.c" 2

# 1 "./IO.h" 1
# 81 "./IO.h"
void init_PMD(void);
void init_IO(void);
void toggleLED( int numLED );
void LED_ON( int numLED );
void LED_OFF( int numLED );
void buzz( int mseconds );
void RC7ON(void);
void RC7OFF(void);
void RA5ON(void);
void RA5OFF(void);
void RELAY_OPEN(void);
void RELAY_CLOSE(void);
# 10 "NRF24L01.c" 2






void initTransceiver( void )
{
    unsigned char ucRegister = 0x00 | 0x20;
    unsigned char ucCommand = 0x02;
    spiWriteRegister( ucRegister, ucCommand );
}





void ceON( void )
{
    RA5ON();
}




void ceOFF( void )
{
    RA5OFF();
}






void RF_sendSPICommand( unsigned char ucCommand )
{
    spiCSActive();

    spiCSInactive();
}
