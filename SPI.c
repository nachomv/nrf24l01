
/*
 * 
 * File:   SPI.c
 * Author: imendizabal
 * Date: 29/03/2020
 * 
 */

#include <xc.h>
#include <pic16f18446.h>
#include "IO.h"
#include "Time.h"
#include "SPI.h"
#include "NRF24L01.h"


/**
 * Initializes the Serial Port Interface (SPI) module
 */
 

void init_SPI(void)
{    
    SSP1CON1bits.SSPEN   = 0; 
    SSP1STATbits.SMP     = 0; /* Input data is sampled at the middle of data output time */
    SSP1STATbits.CKE     = 0; /* Transmit occurs on the transition from Idle to active clock state */
    SSP1CON1bits.WCOL    = 0;
    SSP1CON1bits.SSPOV   = 0;
    SSP1CON1bits.CKP     = 0;
    SSP1CON1bits.SSPM    = 0; /* Clock = Fosc/4 */
    SSP1CON3bits.SCIE    = 1;
    SSP1CON3bits.BOEN    = 1;
    SSP1ADD              = 0x03;
    //SSP1BUF            = 0x00;
    SSP1CON1bits.SSPEN   = 1;
    spiCSInactive();
    delayms(10);
}

/**
 * Receiver Buffer Status
 * @return TRUE a received character has not been read, FALSE otherwise
 */
unsigned char ucBufferStatus(void)
{
    return SSP1STATbits.BF;
}

/**
 * Write Collision Detect Bit
 * @return TRUE a write to SSP1BUF register was attempted while the previous
 * byte was still transmitting
 */
unsigned char ucWriteCollision(void)
{
    return SSP1CON1bits.WCOL;
}

/**
 * Receive Overflow Indicator Bit
 * @return TRUE a byte is received while the SS1BUF register is still holding
 * the previous byte.
 */
unsigned char ucReceiveOverflow(void)
{
    return SSP1CON1bits.SSPOV;
}

/**
 * 
 * @param ucData
 * @return 
 */
unsigned char spiSendByte( unsigned char ucData )
{
    spiCSActive();
    SSP1BUF = ucData;
    spiWait();
    spiCSInactive();
    return SSP1BUF; 
}

/**
 * 
 * @param iFirst
 * @param iSecond
 * @return 
 * 
 */
unsigned char spiRead2Bytes( int *iFirst, int *iSecond )
{
    unsigned int uiLoop = 0;
    spiCSActive();
    spiSendDummyByte();
    spiWait(); 
    //*ucFirst = SSP1BUF;
    *iFirst = SSP1BUF;
    spiSendDummyByte();
    spiWait(); 
    *iSecond = SSP1BUF;
    //*ucSecond = SSP1BUF;
    spiCSInactive();
    return SSP1BUF; 
}

/**
 * 
 * @param ucRegister
 * @param ucData
 * @return 
 */
unsigned char spiWriteRegister( unsigned char ucRegister, unsigned char ucData )
{
    spiCSActive();
    SSP1BUF = ucRegister;
    spiWait();
    SSP1BUF = ucData;
    spiWait();
    spiCSInactive();
    return SSP1BUF;             
}

/**
 * 
 * @param ucRegister
 * @param numBytes
 */
void spiReadRegister( unsigned char ucRegister, int numBytes )
{
    spiCSActive();
    SSP1BUF = ucRegister;
    spiWait();
    int counter = 0;
    for( counter = 0; counter < numBytes; counter++ )
    {
        spiWait();
        spiSendDummyByte();
        spiWait();
    }
    spiCSInactive();
}

/**
 *  Send a dummy byte (0xAA) to generate the Clock signal at SCK pin
 */
void spiSendDummyByte( void )
{
    spiCSActive();
    SSP1BUF = 0xAA;
    spiCSInactive();
}

/**
 * 
 */
void spiWait( void )
{
    while ( !SSP1STATbits.BF );
}

/**
 * 
 * @return 
 */
unsigned int uiSPIDataReady( void )
{
    if( SSP1STATbits.BF )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/**
 * Read the SPI reception buffer
 * @return 
 */
unsigned char ucSPIRead( void )
{
    spiWait();
    return( SSP1BUF );
}

/**
 * Sets the Chip Select (RC7) down (Active)
 */
void spiCSActive( void )
{
    RC7OFF();
}

/**
 * Sets the Chip Select (RC7) up (inactive)
 */
void spiCSInactive( void )
{
     RC7ON();
}
