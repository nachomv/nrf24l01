/*
 * File:   Time.c
 * Author: imendizabal
 *
 * Created on 2 de febrero de 2019, 18:52
 */


#include <xc.h>

#include "Time.h"

void init_Timer0(void)
{
    T0CON1 = 0x4F;
    TMR1H  = 0xFF;
    TMR1L  = 0xFF;
}

void start_Timer0(void)
{
    T0CON0bits.T0EN = 1;
}

void stop_Timer0(void)
{
    T0CON0bits.T0EN = 0;
}

unsigned char status_Timer0(void)
{
    return T0CON0bits.T0EN;
}

void waitCycles( int numCycles )
{
    int counter = 0;
    for( counter = 0; counter < numCycles; counter++ );
}

void delayms( int ms )
{
    unsigned int numLoops = (unsigned int ) (ms*1000 / 6);
    unsigned int uiLoop = 0;
    for( uiLoop = 0; uiLoop < numLoops; uiLoop++ );
}


