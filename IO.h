/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

#ifndef HEADER_IO
#define	HEADER_IO

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

#define LED0_TRIS      TRISAbits.TRISA2
#define LED0_LAT       LATAbits.LATA2
#define LED0_PORT      PORTAbits.RA2
#define LED0_ANS       ANSELAbits.ANSA2

#define ON           1
#define OFF          0

#define LEDST_ON     0
#define LEDST_OFF    1

#define LCD_RS_TRIS    TRISCbits.TRISC0
#define LCD_RS_LAT     LATCbits.LATC0
#define LCD_RS_PORT    PORTCbits.RC0
#define LCD_RS_ANS     ANSELCbits.ANSC0
#define LCD_RS_LAT     LATCbits.LATC0

#define LCD_RW_TRIS    TRISCbits.TRISC1
#define LCD_RW_LAT     LATCbits.LATC1
#define LCD_RW_PORT    PORTCbits.RC1
#define LCD_RW_ANS     ANSELCbits.ANSC1
#define LCD_RW_LAT     LATCbits.LATC1

#define LCD_E_TRIS     TRISCbits.TRISC2
#define LCD_E_LAT      LATCbits.LATC2
#define LCD_E_PORT     PORTCbits.RC2
#define LCD_E_ANS      ANSELCbits.ANSC2
#define LCD_E_LAT      LATCbits.LATC2

#define D4             LATCbits.LATC3
#define D5             LATBbits.LATB5
#define D6             LATAbits.LATA1
#define D7             LATBbits.LATB7

#define INPUT        1
#define OUTPUT       0

void init_PMD(void);
void init_IO(void);
void toggleLED( int numLED );
void LED_ON( int numLED );
void LED_OFF( int numLED );
void buzz( int mseconds );
void RC7ON(void);
void RC7OFF(void);
void RA5ON(void);
void RA5OFF(void);
void RELAY_OPEN(void);
void RELAY_CLOSE(void);


#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif
