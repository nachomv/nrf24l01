
/*
 * File:   NRF24L01.c
 * Author: imendizabal
 * Date:   28/03/2020
 */

#include "NRF24L01.h"
#include "SPI.h"
#include "IO.h"


unsigned char ucRegister = 0x00;
unsigned char ucCommand  = 0x00;

/**
 * 
 */
void initTransceiver( void )
{
    ucRegister = RF_CONFIG | RF_WRITE_FLAG;
    ucCommand  = 0x02; /* PWR_UP = 1 */
    spiWriteRegister( ucRegister, ucCommand );
    
    //ucRegister = RF_SETUP | RF_WRITE_FLAG;
    //ucCommand  = 0x03; /* Output power: 0 dBm */
}

/**
 * 
 */
void ceON( void )
{
    RA5ON();
}

/**
 * 
 */
void ceOFF( void )
{
    RA5OFF();
}


/**
 * 
 * @param ucCommand
 */
void RF_sendSPICommand( unsigned char ucCommand )
{

}

/**
 * 
 */
void RF_readStatus( void )
{
    spiSendByte( RF_NOP );
}

/**
 *
 */
void RF_sendByte( unsigned char ucData )
{
    ucRegister = RF_W_ACK_PAYLOAD;
    spiWriteRegister( ucRegister, ucData );
}

/**
 * Sets the radiofrequency channel
 * @param uiChannel Channel number
 */

void RF_SetChannel( unsigned int uiChannel )
{
    ucRegister = RF_WRITE_FLAG + RF_CHANNEL;
    ucCommand  = (unsigned char) uiChannel;
    spiWriteRegister( ucRegister, ucCommand );
}

/**
 * Gets the current radiofrequency channel
 * @return 
 */
unsigned char RF_GetChannel( void )
{
    
}

/**
 * 
 * @return 
 */
unsigned char RF_ClearIRQ( void )
{
    
}

/**
 * 
 * @param pBuffer
 * @return 
 */
unsigned int RF_readRXLoad( unsigned char *pucBuffer )
{
    
}

/**
 * 
 */
void RF_flushTX( void )
{
    
}


/**
 *
 */
void RF_flushRX( void )
{
    
}

/**
 *
 */
void RF_getFIFOStatus( void )
{
    
}



