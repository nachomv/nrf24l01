
/*
 * File:   NRF24L01.c
 * Author: imendizabal
 * Date:   28/03/2020
 */

#ifndef NRF24L01
#define	NRF24L01

// Register map

#define RF_CONFIG     0x00
#define RF_ENABLE_AA  0x01
#define RF_ENRXADDR   0x02
#define RF_SETUPAW    0x03
#define RF_SETUP_RETR 0x04
#define RF_CHANNEL    0x05
#define RF_SETUP      0x06
#define RF_STATUS     0x07
#define RF_OBSERVE_TX 0x08
#define RF_RPD        0x09
#define RF_RX_ADDR_P0 0x0A
#define RF_RX_ADDR_P1 0x0B
#define RF_RX_ADDR_P2 0x0C
#define RF_RX_ADDR_P3 0x0D
#define RF_RX_ADDR_P4 0x0E
#define RF_RX_ADDR_P5 0x0F
#define RF_TX_ADDR    0x10
#define RF_RX_PW_P0   0x11
#define RF_RX_PW_P1   0x12
#define RF_RX_PW_P2   0x13
#define RF_RX_PW_P3   0x14
#define RF_RX_PW_P4   0x15
#define RF_RX_PW_P5   0x16
#define RF_FIFO_STAT  0x17
#define RF_DYNPD      0x1C
#define RF_FEATURE    0x1D

//Command masks

#define RF_READ_FLAG            0x00
#define RF_WRITE_FLAG           0x20
#define RF_RX_PAYLOAD           0x61
#define RF_TX_PAYLOAD           0xA0
#define RF_FLUSH_TX             0xE1
#define RF_FLUSH_RX             0xE2
#define RF_REUSE_TX_PL          0xE3
#define RF_R_RX_PL_WID          0x60
#define RF_W_ACK_PAYLOAD        0xA8
#define RF_W_TX_PAYLOAD_NOACK   0xB0
#define RF_NOP                  0x00


void initTransceiver( void );
void ceON( void );
void ceOFF( void );
void RF_sendSPICommand( unsigned char ucCommand );
void RF_readStatus( void );
void RF_sendByte( unsigned char ucData );
void RF_SetChannel( unsigned int uiChannel );
unsigned char RF_GetChannel( void );
unsigned char RF_ClearIRQ( void );
unsigned int RF_readRXLoad( unsigned char *pucBuffer );
void RF_flushTX( void );
void RF_flushRX( void );
void RF_getFIFOStatus( void );

#endif
